"""
Home.

Pada menu Home, terdapat 2 (dua) endpoints yang harus dikerjakan oleh 
peserta, yaitu sebagai berikut.

[Get Banner]
- Bobot nilai: 0.5 poin
- URL: /home/banner
- method: GET

Requirements (from the earliest to check):
_ If everything is valid:
    - return {
        "data" : [
   	        {
   		        "id": "uuid",
   		        "image": "/something/image.png",
   		        "title": "lorem ipsum blablabla"
   	        }
        ]
    }

    - status code: 200

[Get Category]
- Bobot nilai: 0.5 poin
- URL: /home/category
- method: GET

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "data" : [
   	        {
   		        "id": "uuid",
   		        "image": "/something/image.png",
   		        "title": "Category A"
   	        }
        ]
    }

    - status code: 200
"""
from flask import Blueprint
from sqlalchemy import Table, MetaData, select

from utils import get_engine, run_query

# this means that you can group all endpoints with prefix "/home" together
home_bp = Blueprint("home", __name__, url_prefix="/home")


@home_bp.route("/banner", methods=["GET"])
def get_banner():
    banners = Table("banners", MetaData(bind=get_engine()), autoload=True)
    get_banner = run_query(select(banners))

    data = []
    for i in range(len(get_banner)):
        dict = {}
        dict["id"] = get_banner[i]["id"]
        dict["image"] = get_banner[i]["image"]
        dict["title"] = get_banner[i]["title"]
        data.append(dict)

    return {"data": data}, 200


@home_bp.route("/category", methods=["GET"])
def get_category():
    categories = Table("categories", MetaData(
        bind=get_engine()), autoload=True)
    get_category = run_query(select(categories))

    data = []
    for i in range(len(get_category)):
        dict = {}
        dict["id"] = get_category[i]["id"]
        dict["image"] = get_category[i]["image"]
        dict["title"] = get_category[i]["title"]
        data.append(dict)

    return {"data": data}, 200
