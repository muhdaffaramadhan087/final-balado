"""
Cart.

Pada section Cart, terdapat 5 (lima) endpoints yang harus diselesaikan oleh
peserta.

Catatan: semua request dari endpoint yang terdapat dalam section “Cart”
akan memiliki headers sebagai berikut:
- Headers:
    - Authentication: string (required) -> Jwt token

[Get User's Carts]
- Bobot nilai: 0.5 poin
- URL: /cart
- method: GET
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "data": [
            {
                "id": "uuid",
                "details": {
                    "quantity": 100,
                    "size": "M"
                },
                "price": 10000,
                "image": "/url/image.jpg",
                "name": "Product a"
            }
        ],
        "total_rows" : 10
    }
    - status code: 200

[Get User's Shipping Address]
- Bobot nilai: 0.5 poin
- URL: /user/shipping_address
- method: GET
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "data":
            {
                "id": "uuid",
                "name": "address name",
                "phone_number": "082713626",
                "address" : "22, ciracas, east jakarta",
                "city": "Jakarta"
            }
    }
    - status code: 200

[Get Shipping Price]
- Bobot nilai: 2 poin
- URL: /shipping_price
- method: GET
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If token does not identify a user:
    - return {"message": "error, Unauthorized user"}
    - status code: 403
- When the cart is empty:
    - return {"message": "error, Cart is empty"}
    - status code: 400
- If everything is valid:
    - Pada endpoint berikut, peserta perlu membuat endpoint untuk menghitung shipping price
    - Dihitung based on cart user yang sekarang
    - Akan ada 2 jenis shipping method:
        - Regular:
            - Jika total harga item < 200: Shipping price merupakan 15% dari total harga item yang dibeli
            - Jika total harga item >= 200: Shipping price merupakan 20% dari total harga item yang dibeli
        - Next Day:
            - Jika total harga item < 300: Shipping price merupakan 20% dari total harga item yang dibeli
            - Jika total harga item >= 300: Shipping price merupakan 25% dari total harga item yang dibeli
    - return {
        "data": [
            {
                "name": "regular",
                "price": 30000
                },
                {
                "name": "next day",
                "price": 20000
            }
        ]
    }
    - status code: 200

[Create Order]
- Bobot nilai: 1.5 poin
- URL: /order
- method: POST
- Request body:
    - shipping_method: string (required) -> Same day
    - shipping_address: dict (required) ->
        {
            "name": "address name",
            "phone_number": "082713626",
            "address" : "22, ciracas, east jakarta",
            "city": "Jakarta"
        }
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If token does not identify a user:
    - return {"message": "error, Unauthorized user"}
    - status code: 403
- If balance is less then total price:
    - return {"message": "error, Please top up <diff>"}
        where <diff> is the difference between buyer's balance and total price of the cart
    - status code: 400
- If everything is valid:
    - Akan mengurangi balance user berdasarkan total harga
    - Akan menghapus semua cart yang ada di user tersebut
    - return {"message": "Order sucess"}
    - status code: 201

[Delete Cart Item]
- Bobot nilai: 1 poin
- URL: /cart/cart_id
- method: DELETE
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If token does not identify a user:
    - return {"message": "error, Unauthorized user"}
    - status code: 403
- If everything is valid:
    - Menghapus salah satu produk dari menu “Cart”
    - return {"message": "Cart deleted"}
    - status code: 200
"""
import uuid
import datetime

from flask import Blueprint, request
from sqlalchemy import Table, MetaData, select, update, insert, delete, and_

from product_detail_page import cart_bp
from utils import run_query, get_engine

# this means that you can group all endpoints with prefix "/user" together
user_bp = Blueprint("user", __name__, url_prefix="/user")

# this means that you can group all endpoints with prefix "/shipping_price" together
shipping_price_bp = Blueprint(
    "shipping_price", __name__, url_prefix="/shipping_price")

# this means that you can group all endpoints with prefix "/order" together
order_bp = Blueprint("order", __name__, url_prefix="/order")


@cart_bp.route("", methods=["GET"])
def get_user_cart():
    headers = request.headers
    Authentication = headers.get("Authentication")

    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)

    cart = run_query(select(carts).where(
        and_(carts.c.token == Authentication, carts.c.status == "cart")))

    data = []
    for i in range(len(cart)):
        dict = {}
        dict["id"] = cart[i]["id"]

        size = run_query(select(sizes).where(sizes.c.id == cart[i]["size_id"]))
        product = run_query(select(products).where(
            products.c.id == size[0]["product_id"]))
        image = run_query(select(images).where(
            images.c.product_id == size[0]["product_id"]))

        dict["details"] = {
            "quantity": cart[i]["quantity"],
            "size": size[0]["size"]
        }

        dict["price"] = product[0]["price"]

        dict["image"] = []
        for i in range(len(image)):
            dict["image"].append(image[i]["images_url"])

        if len(image) <= 1:
            dict["image"] = dict["image"][0]

        dict["name"] = product[0]["title"]

        data.append(dict)

    return {
        "data": data,
        "total_rows": len(data)
    }, 200


@user_bp.route("/shipping_address", methods=["GET"])
def get_user_sa():
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    shipping_addresses = Table("shipping_addresses", MetaData(
        bind=get_engine()), autoload=True)
    user = run_query(select(users).where(users.c.token == Authentication))
    shipping_address = run_query(
        select(shipping_addresses).where(shipping_addresses.c.token == Authentication))

    return {
        "data":
            {
                "id": shipping_address[0]["id"],
                "name": shipping_address[0]["name"],
                "phone_number": user[0]["phone_number"],
                "address": shipping_address[0]["address"],
                "city": shipping_address[0]["city"]
            }
    }, 200


@shipping_price_bp.route("", methods=["GET"])
def get_shipping_price():
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "buyer")))
    cart = run_query(select(carts).where(
        and_(carts.c.token == Authentication, carts.c.status == "cart")))

    if len(user) == 0:
        return {"message": "error, Unauthorized user"}, 403
    elif len(cart) == 0:
        return {"message": "error, Cart is empty"}, 400
    else:
        data = []

        for i in range(len(cart)):
            size = run_query(select(sizes).where(
                sizes.c.id == cart[i]["size_id"]))
            product = run_query(select(products).where(
                products.c.id == size[0]["product_id"]))

            total_price = cart[i]["quantity"] * product[0]["price"]
            shipping_method = cart[i]["shipping_method"]
            shipping_price = 0

            if shipping_method == "regular" or shipping_method == "same day":
                if total_price < 200000:
                    shipping_price = 0.15 * total_price
                else:
                    shipping_price = 0.2 * total_price

            if shipping_method == "next day":
                if total_price < 300000:
                    shipping_price = 0.2 * total_price
                else:
                    shipping_price = 0.25 * total_price

            run_query(update(carts).values({"shipping_price": int(shipping_price)}).where(
                carts.c.id == cart[i]["id"]), commit=True)

            data.append({"name": shipping_method,
                        "price": int(shipping_price)})

        return {"data": data}, 200


@order_bp.route("", methods=["POST"])
def create_order():
    body = request.json
    headers = request.headers
    shipping_method = body.get("shipping_method")
    shipping_address = body.get("shipping_address")
    name = shipping_address.get("name")
    phone_number = shipping_address.get("phone_number")
    address = shipping_address.get("address")
    city = shipping_address.get("city")
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    shipping_addresses = Table("shipping_addresses", MetaData(
        bind=get_engine()), autoload=True)

    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "buyer")))
    cart = run_query(select(carts).where(
        and_(carts.c.token == Authentication, carts.c.status == "cart")))
    shipping_addresse = run_query(select(shipping_addresses).where(
        shipping_addresses.c.token == Authentication))

    if len(user) == 0:
        return {"message": "error, Unauthorized user"}, 403

    total_price = 0

    for i in range(len(cart)):
        size = run_query(select(sizes).where(
            sizes.c.id == cart[i]["size_id"]))
        product = run_query(select(products).where(
            products.c.id == size[0]["product_id"]))

        price = cart[i]["quantity"] * product[0]["price"]

        total_price += price + cart[i]["shipping_price"]

    diff = total_price - user[0]["balance"]

    if diff > 0:
        return {"message": f"error, Please top up {diff}"}, 400
    else:
        x = datetime.datetime.now()

        run_query(update(users).values({"balance": abs(diff), "phone_number": phone_number}).where(
            users.c.token == Authentication), commit=True)
        run_query(update(carts).values(
            {"shipping_method": shipping_method, "status": "waiting", "created_at": x.strftime("%a, %d %B %Y"), "total_price": total_price}).where(carts.c.id == cart[0]["id"]), commit=True)

        if len(shipping_addresse) == 0:
            run_query(insert(shipping_addresses).values(
                {"id": f"{uuid.uuid4()}", "name": name, "address": address, "city": city, "token": Authentication}), commit=True)
        else:
            run_query(update(shipping_addresses).values(
                {"name": name, "address": address, "city": city}).where(shipping_addresses.c.token == shipping_addresse[0]["token"]), commit=True)
            seller = run_query(select(users).where(users.c.type == "seller"))
            run_query(update(users).values({"balance": seller[0]["balance"]+total_price}).where(
                users.c.type == "seller"), commit=True)

        return {"message": "Order sucess"}, 201


@cart_bp.route("/<cart_id>", methods=["DELETE"])
def delete_cart_item(cart_id):
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "buyer")))

    if len(user) == 0:
        return {"message": "error, Unauthorized user"}, 403
    else:
        run_query(delete(carts).where(carts.c.id == cart_id), commit=True)
        return {"message": "Cart deleted"}, 200
