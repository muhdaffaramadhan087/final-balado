"""
Admin Page.

Terdapat total 4 (empat) endpoints baru yang perlu dibuat oleh peserta
dalam Admin Page beserta 2 (dua) endpoints tambahan yang telah dibuat
pada section sebelumnya.

Catatan: semua yang ada di dalam admin page hanya bisa diakses oleh user
dengan level admin dan memiliki headers sebagai berikut.
- Headers:
    - Authentication: string (required) -> Jwt token

[Get Orders]
- Bobot nilai: 1 poin
- URL: /orders
- method: GET
- Query Parameter:
    - sort_by: string (required) -> Price a_z, Price z_a
    - page: integer (required) -> 1
    - page_size: integer (required) -> 25
    - is_admin: boolean (required) -> True
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- if query_params "is_admin" is false:
    - return {"message": "error, user is not admin"}
    - status code: 400
- If everything is valid:
    - When query_params “is_admin” is true, return all orders from all user, with additional key:
        - Created_at
        - User_id
        - email
    - return {
        "data": [
            {
                "id": "uuid",
                "title": "Nama product",
                "size": [
                    "S",
                    "M",
                    "L"
                ],
                "created_at": "Tue, 25 august 2022",
                "product_detail": "lorem ipsum",
                "email": "raihan@gmail.com",
                "images_url": [
                    "/image/image1",
                    "/image/image2"
                ],
                "user_id": "uuid",
                "total": 1000
            }
        ]
    }
    - status code: 200

[Get Product]
Sudah ada di product_list.py

[Create Product]
- Bobot nilai: 2 poin
- URL: /products
- method: POST
- Request body:
    - product_name: string (required) -> Product 1
    - description: string (required) -> Lorem ipsum
    - images: list (required) -> [image_1, image_2, image_3]
    - condition: string (required) -> new
    - category: integer (required) -> category_id
    - price: integer (required) -> 10000
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - Menambahkan sebuah product melalui admin page.
    - return {"message" : "Product added"}
    - status code: 201

[Update Product]
- Bobot nilai: 1 poin
- URL: /products
- method: PUT
- Request body:
    - product_name: string (required) -> Product 1
    - description: string (required) -> Lorem ipsum
    - images: list (required) -> [image_1, image_2, image_3]
    - condition: string (required) -> new
    - category: integer (required) -> category_id
    - price: integer (required) -> 10000
    - product_id: integer (required) -> product_id
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- Jika ada produk dengan name, category dan condition yang sama persis:
    - return {"message": "error, Product is already exists"}
    - status code: 409
- If everything is valid:
    - Melakukan perubahan pada item product yang sebelumnya telah dibuat.
    - return {"message": "Product updated"}
    - status code: 200
- NOTE:
    Produk unique berdasarkan: name, category, condition.

[Delete Product]
- Bobot nilai: 1 poin
- URL: /products/:product_id
- method: DELETE
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - Menghapus item product yang sebelumnya telah dibuat.
    - return {"message": "Product deleted"}
    - status code: 200
- NOTE:
    Hanya berupa “soft delete” jadi semua orders/pesanan yang
    terdapat product tersebut tidak terhapus juga.

[Get Category]
Sudah ada di product_list.py

[Create Category]
- Bobot nilai: 1 poin
- URL: /categories
- method: POST
- Request body:
    - category_name: string (required) -> Category A
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If Category name is exists:
    - return {"message": "error, Category is already exists"}
    - status code: 409
- If everything is valid:
    - return {"message": "Category added"}
    - status code: 201
- NOTE:
    Category is unique by name

[Update Category]
- Bobot nilai: 1 poin
- URL: /categories/{category_id}
- method: PUT
- Request body:
    - category_name: string (required) -> Category A
    - category_id: integer (required) -> category_id
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If Category name is exists:
    - return {"message": "error, Category is already exists"}
    - status code: 409
- If everything is valid:
    - return {"message": "Category updated"}
    - status code: 200
- NOTE:
    Category unique by name

[Delete Category]
- Bobot nilai: 1 poin
- URL: /categories/{category_id}
- method: DELETE
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - return {"message": "Category deleted"}
    - status code: 200
- NOTE:
    Hanya berupa “soft delete” jadi semua orders/pesanan yang
    terdapat product tersebut tidak terhapus juga.

[Get Total Sales]
- Bobot nilai: 1 poin
- URL: /sales
- method: GET
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "data": {
            "total": 100000
            }
    }
    - status code: 200
"""
import uuid
import base64
from io import BytesIO
import random
import string
from PIL import Image

from flask import Blueprint, request, json
from sqlalchemy import Table, MetaData, select, insert, update, delete, and_, asc, desc
from utils import run_query, get_engine
from product_list import products_bp, categories_bp

# this means that you can group all endpoints with prefix "/orders" together
orders_bp = Blueprint("orders", __name__, url_prefix="/orders")

# this means that you can group all endpoints with prefix "/sales" together
sales_bp = Blueprint("sales", __name__, url_prefix="/sales")


@orders_bp.route("", methods=["GET"])
def get_orders():
    args = request.args
    headers = request.headers
    sort_by = args.get("sort_by")
    page = args.get("page")
    page_size = args.get("page_size")
    is_admin = args.get("is_admin", type=bool)
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)

    if sort_by == "price a_z":
        sort_by = asc(carts.c.total_price)
    else:
        sort_by = desc(carts.c.total_price)

    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))
    cart = run_query(select(carts).where(carts.c.status != "cart").limit(
        int(page)*int(page_size)).order_by(sort_by))

    if len(user) == 0 and is_admin != True:
        return {"message": "error, user is not admin"}, 400

    else:
        data = []
        for i in range(len(cart)):
            size = run_query(select(sizes).where(
                sizes.c.id == cart[i]["size_id"]))
            product = run_query(select(products).where(
                products.c.id == size[0]["product_id"]))
            all_size = run_query(select(sizes).where(
                sizes.c.product_id == product[0]["id"]))
            buyer = run_query(select(users).where(
                users.c.token == cart[i]["token"]))
            image = run_query(select(images).where(
                images.c.product_id == product[0]["id"]))

            sz = []
            for s in range(len(all_size)):
                sz.append(all_size[s]["size"])

            img = []
            for m in range(len(image)):
                img.append(image[m]["images_url"])

            dict = {
                "id": cart[i]["id"],
                "title": product[0]["title"],
                "size": sz,
                "created_at": cart[i]["created_at"],
                "product_detail": product[0]["product_detail"],
                "email": buyer[0]["email"],
                "images_url": img,
                "user_id": buyer[0]["id"],
                "total": cart[i]["quantity"]
            }
            data.append(dict)

        return {"data": data}, 200


@products_bp.route("", methods=["POST"])
def create_product():
    body = json.loads(request.data)
    headers = request.headers
    product_name = body.get("product_name")
    description = body.get("description")
    images_list = body.get("images")
    condition = body.get("condition")
    category = body.get("category")
    price = body.get("price")
    Authentication = headers.get("Authentication")

    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)
    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400

    data = {"id": str(uuid.uuid4()), "title": product_name, "product_detail": description,
            "condition": condition, "category_id": category, "price": price}
    size_data = [
        {
            "id": str(uuid.uuid4()),
            "product_id": data["id"],
            "size": 'S'
        },
        {
            "id": str(uuid.uuid4()),
            "product_id": data["id"],
            "size": 'M'
        },
        {
            "id": str(uuid.uuid4()),
            "product_id": data["id"],
            "size": 'L'
        },
    ]

    run_query(insert(products).values(data), commit=True)
    run_query(insert(sizes).values(size_data), commit=True)
    img_list = []
    for image in images_list:
        base64_bytes = image.split(',')
        imag = bytes(base64_bytes[1], encoding="ascii")
        file_name = ''.join(random.choices(string.ascii_lowercase + string.digits, k=20)) + f".{base64_bytes[0].split('/')[1].split(';')[0]}"
        img = Image.open(BytesIO(base64.b64decode(imag)))
        img.save('images/'+f'{file_name}')
        img_list.append(file_name)
    for i in range(len(img_list)):
        run_query(insert(images).values(
            {"images_url": img_list[i], "product_id": data["id"]}), commit=True)

    return {"message": "Product added"}, 201


@products_bp.route("", methods=["PUT"])
def update_product():
    body = json.loads(request.data)
    headers = request.headers
    product_name = body.get("product_name")
    description = body.get("description")
    images_list = body.get("images")
    condition = body.get("condition")
    category = body.get("category")
    price = body.get("price")
    product_id = body.get("product_id")
    Authentication = headers.get("Authentication")

    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)
    users = Table("users", MetaData(bind=get_engine()), autoload=True)

    product = run_query(select(products).where(and_(products.c.title == product_name,
                        products.c.category_id == category, products.c.condition == condition)))
    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400
    elif len(product) != 0:
        return {"message": "error, Product is already exists"}, 409
    else:
        data = {"title": product_name, "product_detail": description,
                "condition": condition, "category_id": category, "price": price}

        run_query(update(products).values(data).where(
            products.c.id == product_id), commit=True)

        run_query(delete(images).where(
            images.c.product_id == product_id), commit=True)

        img_list = []
        for image in images_list:
            base64_bytes = image.split(',')
            imag = bytes(base64_bytes[1], encoding="ascii")
            file_name = ''.join(random.choices(string.ascii_lowercase + string.digits, k=20)) + f".{base64_bytes[0].split('/')[1].split(';')[0]}"
            img = Image.open(BytesIO(base64.b64decode(imag)))
            img.save('images/'+f'{file_name}')
            img_list.append(file_name)

        for i in range(len(img_list)):
            run_query(insert(images).values(
                {"images_url": img_list[i], "product_id": product_id}), commit=True)

        return {"message": "Product updated"}, 200


@products_bp.route("/<product_id>", methods=["DELETE"])
def delete_product(product_id):
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)

    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400

    run_query(delete(products).where(products.c.id == product_id), commit=True)
    run_query(delete(images).where(
        images.c.product_id == product_id), commit=True)

    return {"message": "Product deleted"}, 200


@categories_bp.route("", methods=["POST"])
def create_category():
    headers = request.headers
    Authentication = headers.get("Authentication")
    body = json.loads(request.data)
    category_name = body.get("category_name")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400
    else:
        categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
        category = run_query(select(categories).where(categories.c.title == category_name))
        if len(category) != 0:
            return {"message": "error, Category is already exists"}, 409
        else:
            id = str(uuid.uuid4())
            categories = Table("categories", MetaData(bind=get_engine()), autoload=True)
            run_query(insert(categories).values({"id": id, "title": category_name}), commit=True)
    return {"message": "Category added"}, 201


@categories_bp.route("/<category_id>", methods=["PUT"])
def update_category(category_id):
    headers = request.headers
    Authentication = headers.get("Authentication")
    body = json.loads(request.data)
    category_name = body.get("category_name")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400
    else:
        categories = Table("categories", MetaData(
            bind=get_engine()), autoload=True)
        categoryByName = run_query(select(categories).where(
            (categories.c.title == category_name)))
        if len(categoryByName) != 0:
            return {"message": "error, Category is already exists"}, 409
        else:
            run_query(update(categories).values({"title": category_name}).where(
                categories.c.id == category_id), commit=True)
            return {"message": "Category updated"}, 200


@categories_bp.route("/<category_id>", methods=["DELETE"])
def delete_category(category_id):
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400
    else:
        categories = Table("categories", MetaData(
            bind=get_engine()), autoload=True)
        categoryById = run_query(select(categories).where(
            categories.c.id == category_id))
        if len(categoryById) == 0:
            return {"message": "error, id is invalid"}, 400
        else:
            products = Table("products", MetaData(
                bind=get_engine()), autoload=True)
            run_query(update(products).values({"category_id": ""}).where(
                products.c.category_id == category_id), commit=True)
            run_query(delete(categories).where(
                categories.c.id == category_id), commit=True)
            return {"message": "Category deleted"}, 200


@sales_bp.route("", methods=["GET"])
def get_total_sales():
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "seller")))

    if len(user) == 0:
        return {"message": "error, user is not admin"}, 400

    return {
        "data": {
            "total": user[0]["balance"]
        }
    }, 200
