"""
Universal.

Terdapat total 28 endpoints yang memiliki bobot nilai bervariasi dengan total 29 poin nilai.
Keberhasilan penyelesaian endpoints akan masuk ke dalam kriteria penilaian “API”.

TO DO:
    - Implement setup logic below (on create_app function)
    - Implement ALL the endpoints with IMPLEMENT THIS
    - You may optionally implement additional functions or classes as you see fit

Terdapat 1 (satu) endpoint universal yang perlu dibuat oleh peserta, yaitu endpoint untuk mengambil atau memunculkan gambar dalam platform.

[Get Image]
- Bobot nilai: 1 poin
- URL: /image/{image_name.extension}
- method: GET

Requirements (from the earliest to check):
- If everything is valid:
    - return image file
    - status code: 200

For any error messages, you should return with this response.
    - return {"message" : "error, user already exists"}
    - status code: 409
"""
import os

from flask import Flask, send_file
from sqlalchemy import Column, String, Integer, CHAR, MetaData, Table, inspect, Text, Boolean

from home import home_bp
from authentication import signup_bp, signin_bp
from product_list import products_bp, categories_bp
from product_detail_page import cart_bp, products_bp
from cart import user_bp, shipping_price_bp, order_bp, cart_bp
from profile_page import user_bp, order_bp
from admin_page import orders_bp, sales_bp, products_bp, categories_bp
from utils import get_engine, run_query
from flask_cors import CORS
from seed import create_seeder
from sqlalchemy import delete

def create_app():
    app = Flask(__name__)
    CORS(app)

    # always register your blueprint(s) when creating application
    blueprints = [orders_bp, sales_bp, signup_bp, signin_bp, user_bp, shipping_price_bp,
                  order_bp, home_bp, cart_bp, products_bp, categories_bp]
    for blueprint in blueprints:
        app.register_blueprint(blueprint)

    engine = get_engine()

    # - create necessary tables
    if not inspect(engine).has_table('banners'):
        meta = MetaData()
        banners = Table(
            'banners',
            meta,
            Column('id', String),
            Column('image', String),
            Column('title', String(128)),
            Column('deleted', Boolean, default = False),
        )
        meta.create_all(engine)
    else:
        banners = Table("banners", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(banners), commit=True)

    if not inspect(engine).has_table('categories'):
        meta = MetaData()
        categories = Table(
            'categories',
            meta,
            Column('id', String, primary_key=True, unique= True),
            Column('image', String),
            Column('deleted', Boolean, default = False),
            Column('title', String(128))
        )
        meta.create_all(engine)
    else:
        categories = Table("categories", MetaData(bind=get_engine()), autoload=False)
        run_query(delete(categories), commit=True)

    if not inspect(engine).has_table('users'):
        meta = MetaData()
        users = Table(
            'users',
            meta,
            Column('id', String(128), primary_key=True),
            Column('name', String(128), unique=True),
            Column('email', String(128)),
            Column('password', String(128)),
            Column('phone_number', String(128)),
            Column('token', Text, unique=True),
            Column('type', String(12)),
            Column('balance', Integer)
        )
        meta.create_all(engine)
    else:
        users = Table("users", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(users), commit=True)


    if not inspect(engine).has_table('sizes'):
        meta = MetaData()
        sizes = Table(
            'sizes',
            meta,
            Column('id', String, primary_key=True),
            Column('product_id', String),
            Column('size', CHAR)
        )
        meta.create_all(engine)
    else:
        sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(sizes), commit=True)

    if not inspect(engine).has_table('shipping_addresses'):
        meta = MetaData()
        shipping_addresses = Table(
            'shipping_addresses',
            meta,
            Column('id', String(128), unique=True),
            Column('name', String(128)),
            Column('phone_number', String(128)),
            Column('address', String(128)),
            Column('city', String(64)),
            Column('token', Text)
        )
        meta.create_all(engine)
    else:
        shipping_addresses = Table("shipping_addresses", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(shipping_addresses), commit=True)

    if not inspect(engine).has_table('products'):
        meta = MetaData()
        products = Table(
            'products',
            meta,
            Column('id', String, primary_key=True),
            Column('title', String(128)),
            Column('image', String),
            Column('price', Integer),
            Column('category_id', String(128)),
            Column('product_detail', String(128)),
            Column('condition', String(128)),
            Column('deleted', Boolean, default = False),
            Column('sold', Integer)
        )
        meta.create_all(engine)
    else:
        products = Table("products", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(products), commit=True)

    if not inspect(engine).has_table('sizes'):
        meta = MetaData()
        sizes = Table(
            'sizes',
            meta,
            Column('id', String, primary_key=True),
            Column('product_id', String),
            Column('size', CHAR)
        )
        meta.create_all(engine)
    else:
        sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(sizes), commit=True)

    if not inspect(engine).has_table('images'):
        meta = MetaData()
        images = Table(
            'images',
            meta,
            Column('product_id', String),
            Column('images_url', String(128))
        )
        meta.create_all(engine)
    else:
        images = Table("images", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(images), commit=True)

    if not inspect(engine).has_table('carts'):
        meta = MetaData()
        carts = Table(
            'carts',
            meta,
            Column('id', String, primary_key=True),
            Column('quantity', Integer),
            Column('token', String(128)),
            Column('shipping_method', String(64)),
            Column('shipping_price', Integer),
            Column('size_id', String(128)),
            Column('created_at', String(128)),
            Column('status', String(128)),
            Column('total_price', Integer)
        )
        meta.create_all(engine)
    else:
        carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
        run_query(delete(carts), commit=True)
    
    

    create_seeder()

    # Universal Endpoints
    @app.route("/image/<image_name>", methods=["GET"])
    def get_image(image_name):
        extension = image_name.split(".")
        extension = extension[len(extension)-1]
        return send_file('images/'+image_name, mimetype=f"image/{extension}"), 200

    @app.errorhandler(Exception)
    def handle_error(e: Exception):
        return (({"message": "error, " + str(e)}), 404)

    return app


app = create_app()

# Uncomment this before deploy
# app.run(debug=True)
