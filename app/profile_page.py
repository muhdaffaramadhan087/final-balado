"""
Profile Page.

Terdapat 5 (lima) endpoints baru yang perlu diselesaikan oleh peserta, yaitu
    1) User Details;
    2) Change Shipping Address;
    3) Top-up Balance;
    4) Get User Balance;
    5) User Orders.

Selain itu, pada halaman ini, peserta juga perlu memanggil kembali
endpoint yang sebelumnya sudah dibuat: “Get User Shipping Address”.

Semua endpoints dalam Profile Page harus memiliki headers berikut.
- Headers:
    - Authentication: string (required) -> Jwt token

[User Details]
- Bobot nilai: 1 poin
- URL: /user
- method: GET
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "data":
            {
                "name": "Raihan Parlaungan",
                "email": "raihan@gmail.com",
                "phone_number": "081380737126"
            }
    }
    - status code: 200

[Change Shipping Address]
- Bobot nilai: 1 poin
- URL: /user/shipping_address
- method: POST
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "name": "address name",
        "phone_number": "082713626",
        "address" : "22, ciracas, east jakarta",
        "city": "Jakarta"
    }
    - status code: 200

[Top-up Balance]
- Bobot nilai: 1 poin
- URL: /user/balance
- method: POST
- Query Parameter:
    - amount: integer (required) -> 10000
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - Menambahkan balance pada akun user yang digunakan untuk pembelian.
    - return {"message": "Top Up balance success"}
    - status code: 200

[Get User Balance]
- Bobot nilai: 1 poin
- URL: /user/balance
- method: GET
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - Menampilkan current balance yang dimiliki oleh user.
    - return {
        "data":
            {
                "balance": 10000
            }
    }
    - status code: 200

[Get User Shipping Address]
Sudah ada di cart.py

[User Orders]
- Bobot nilai: 1 poin
- URL: /order
- method: GET
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- If everything is valid:
    - Berupa informasi mengenai pesanan yang telah dilakukan oleh pengguna.
    - return {
        "data": [
            {
                "id": "uuid",
                "created_at": "Mon, 22 august 2022",
                "products": [
                    {
                        "id": "uuid",
                        "details": {
                            "quantity": 100,
                            "size": "M"
                        },
                        "price": 10000,
                        "image": "/url/image.jpg",
                        "name": "Product a"
                    }
                ],
                "shipping_method": "same day",
                "status": "waiting",
                "shipping_address": {
                    "name": "address name",
                    "phone_number": "082713626",
                    "address": "22, ciracas, east jakarta",
                    "city": "Jakarta"
                }
            }
        ]
    }
    - status code: 200
- NOTE:
    “price” dari products disini adalah harga asli dari product * jumlah
    product yang di checkout.
"""
from flask import request
from sqlalchemy import Table, MetaData, select, update, and_, insert

from cart import user_bp, order_bp
from utils import run_query, get_engine

import uuid


@user_bp.route("", methods=["GET"])
def user_details():
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    data = run_query(select(users).where(users.c.token == Authentication))

    return {
        "data":
            {
                "name": data[0]["name"],
                "email": data[0]["email"],
                "phone_number": data[0]["phone_number"]
            }
    }, 200


@user_bp.route("/shipping_address", methods=["POST"])
def change_sa():
    body = request.json
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    shipping_addresses = Table("shipping_addresses", MetaData(
        bind=get_engine()), autoload=True)
    shipping_address = run_query(
        select(shipping_addresses).where(shipping_addresses.c.token == Authentication))
    body["id"] = str(uuid.uuid4())
    body["token"]= Authentication
    if len(shipping_address) == 0:
        run_query(insert(shipping_addresses).values(body), commit=True)
    else:
        run_query(update(shipping_addresses).values(body).where(
            shipping_addresses.c.token == Authentication), commit=True)
        
    user = run_query(select(users).where(users.c.token == Authentication))
    shipping_address = run_query(
        select(shipping_addresses).where(shipping_addresses.c.token == Authentication))

    return {
        "name": shipping_address[0]["name"],
        "phone_number": shipping_address[0]["phone_number"],
        "address": shipping_address[0]["address"],
        "city": shipping_address[0]["city"]
    }, 200


@ user_bp.route("/balance", methods=["POST"])
def top_up_balance():
    args = request.get_json(True)
    headers = request.headers
    Authentication = headers.get("Authentication")
    amount = args.get("amount")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    user = run_query(select(users).where(users.c.token == Authentication))
    run_query(update(users).values({"balance": user[0]["balance"] + int(amount)}).where(
        users.c.token == Authentication), commit=True)

    return {"message": "Top Up balance success"}, 200


@ user_bp.route("/balance", methods=["GET"])
def get_user_balance():
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    data = run_query(select(users).where(
        users.c.token == Authentication))

    return {
        "data":
            {
                "balance": data[0]["balance"]
            }
    }, 200


@ order_bp.route("", methods=["GET"])
def user_orders():
    headers = request.headers
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)
    shipping_addresses = Table("shipping_addresses", MetaData(
        bind=get_engine()), autoload=True)

    cart = run_query(select(carts).where(
        and_(carts.c.token == Authentication, carts.c.status != "cart")))
    shipping_address = run_query(select(shipping_addresses).where(
        shipping_addresses.c.token == Authentication))
    user = run_query(select(users).where(
        users.c.token == Authentication))

    data = []
    for i in range(len(cart)):
        size = run_query(select(sizes).where(sizes.c.id == cart[i]["size_id"]))
        product = run_query(select(products).where(
            products.c.id == size[0]["product_id"]))

        prods = []

        for x in range(len(product)):
            image = run_query(select(images).where(
                images.c.product_id == product[x]["id"]))
            img = []
            for n in range(len(image)):
                img.append(image[n]["images_url"])

            if len(img) <= 1:
                img = img[0]

            prod = {
                "id": product[x]["id"],
                "details": {
                    "quantity": cart[i]["quantity"],
                    "size": size[0]["size"],
                },
                "price": cart[i]["quantity"] * product[x]["price"],
                "image": img,
                "name": product[x]["title"]
            }
            prods.append(prod)

        dict = {
            "id": cart[i]["id"],
            "created_at": cart[i]["created_at"],
            "products": prods,
            "shipping_method": cart[i]["shipping_method"],
            "status": cart[i]["status"],
            "shipping_address": {
                "name": shipping_address[0]["name"],
                "phone_number": user[0]["phone_number"],
                "address": shipping_address[0]["address"],
                "city": shipping_address[0]["city"]
            }
        }

        data.append(dict)

    return {"data": data}, 200
