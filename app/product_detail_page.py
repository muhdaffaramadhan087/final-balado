"""
Product Detail Page.

Pada halaman product detail, akan ada 2 (dua) endpoints yang perlu
diselesaikan oleh peserta, sebagai berikut:

[Get Product Details]
- Bobot nilai: 1 poin
- URL: /products/{id}
- method: GET

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "id": "uuid",
        "title":"Nama product",
        "size":["S", "M", "L"],
        "product_detail":"lorem ipsum",
        "price": 1000,
        "images_url": ["/image/image1", "/image/image2"],
    }
    - status code: 200

[Add to Cart]
- Bobot nilai: 2 poin
- URL: /cart
- method: POST
- Request body:
    - id: string (required) -> item_id
    - quantity: integer (required) -> 10
    - size: character (required) -> M
- Headers:
    - Authentication: string (required) -> Jwt token

Requirements (from the earliest to check):
- Hanya bisa di panggil oleh user yang sudah login, jika belum login
    - return {"message": "error, Unauthorized user"}
    - status code: 403
- If everything is valid:
    - Items di cart bersifat unique berdasarkan item_id dan size
      Jika menambahkan item yang mempunyai id dan size yang sama ke 
      dalam cart, akan menambahkan quantity item di cart tersebut
    - return {"message" : "Item added to cart"}
    - status code: 201
"""
import uuid

from flask import Blueprint, request
from sqlalchemy import Table, MetaData, select, insert, update, and_
from utils import get_engine, run_query
from product_list import products_bp

# this means that you can group all endpoints with prefix "/cart" together
cart_bp = Blueprint("cart", __name__, url_prefix="/cart")


@products_bp.route("/<id>", methods=["GET"])
def get_product_details(id):
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)

    product = run_query(select(products).where(products.c.id == id))
    image = run_query(select(images).where(images.c.product_id == id))
    size = run_query(select(sizes).where(sizes.c.product_id == id))

    size_list = []
    images_url = []

    for i in range(len(size)):
        size_list.append(size[i]["size"])

    for i in range(len(image)):
        images_url.append(image[i]["images_url"])

    if len(size_list) <= 1:
        size_list = size_list[0]

    if len(images_url) <= 1:
        images_url = images_url[0]

    return {
        "id": product[0]["id"],
        "title": product[0]["title"],
        "size": size_list,
        "product_detail": product[0]["product_detail"],
        "price": product[0]["price"],
        "images_url": images_url
    }, 200


@cart_bp.route("", methods=["POST"])
def add_to_cart():
    body = request.json
    headers = request.headers
    id = body.get("id")
    quantity = body.get("quantity")
    size = body.get("size")
    Authentication = headers.get("Authentication")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    sizes = Table("sizes", MetaData(bind=get_engine()), autoload=True)
    carts = Table("carts", MetaData(bind=get_engine()), autoload=True)
    

    user = run_query(select(users).where(
        and_(users.c.token == Authentication, users.c.type == "buyer")))
    size = run_query(select(sizes).where(
        and_(sizes.c.product_id == id, sizes.c.size == size)))
    try:
        cart = run_query(select(carts).where(and_(carts.c.size_id == size[0]["id"], carts.c.token == Authentication, carts.c.status == "cart")))
    except:
        cart = []
    
    if len(user) == 0:
        return {"message": "error, Unauthorized user"}, 403
    elif len(cart) == 0:
        data = {"id": f"{uuid.uuid4()}", "quantity": quantity,
                "size_id": size[0]["id"], "status": "cart"}
        run_query(insert(carts).values(data), commit=True)
    else:
        run_query(update(carts).values(
            {"quantity": cart[0]["quantity"]+quantity}).where(and_(carts.c.token == Authentication, carts.c.size_id == size[0]["id"], carts.c.status == "cart")), commit=True)
    return {"message": "Item added to cart"}, 201
