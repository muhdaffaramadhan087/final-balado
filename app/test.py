from utils import get_engine, run_query
from flask import Blueprint, request
from sqlalchemy import (
    MetaData,
    Table,
    delete,
    insert,
    select,
)
from sqlalchemy.exc import IntegrityError


test_bp = Blueprint("test", __name__, url_prefix="/test")

@test_bp.route("", methods=["GET"])
def test():
    return {"message": "Testing!"}
