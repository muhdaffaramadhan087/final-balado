"""
Authentication.

Untuk melakukan authentication, terdapat 2 (dua) endpoints yang harus
dikerjakan oleh peserta, yaitu Sign-up dan Sign-in sebagai berikut:

[Sign-up]
- Bobot nilai: 1 poin
- URL: /sign-up
- method: POST
- Request body:
    - name: string (required) -> Raihan Parlaungan
    - email: string (required) -> raihan@gmail.com
    - phone_number: string (required) -> 081380737126
    - password: string (required) -> password1234

Requirements (from the earliest to check):
- If everything is valid:
    - Jika tidak diberikan type pada request body maka type otomatis menjadi buyer
    - return {"message" : "success, user created"}
    - status code: 201

[Sign-in]
- Bobot nilai: 1 poin
- URL: /sign-in
- method: POST
- Request body:
    - email: string (required) -> raihan@gmail.com
    - password: string (required) -> password1234

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "user_information" : [
            {
                "name": "Raihan Parlaungan",
                "email": "raihan@gmail.com",
                "phone_number": "081380737126",
                "type" : "buyer"( there are 2 types of user, "buyer" and "seller"
            }
        ],
        "token" : <jwt_token>,
        "message" : "Login success"
    }
    - status code: 200
"""
import secrets
import jwt
import uuid

from flask import Blueprint, request
from sqlalchemy import Table, MetaData, insert, select, update
from passlib.hash import sha256_crypt
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine

# this means that you can group all endpoints with prefix "/sign-up" together
signup_bp = Blueprint("signup", __name__, url_prefix="/sign-up")

# this means that you can group all endpoints with prefix "/sign-in" together
signin_bp = Blueprint("signin", __name__, url_prefix="/sign-in")


@signup_bp.route("", methods=["POST"])
def sign_up():
    body = request.json
    password = body.get("password")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    try:
        hash = sha256_crypt.hash(password)
        body["type"] = "buyer"
        body["password"] = hash
        body["balance"] = 0
        body["id"] = str(uuid.uuid4())
        run_query(insert(users).values(body), commit=True)
        return {"message": "success, user created"}, 201
    except IntegrityError:
        return {"message": "error, user already exists"}, 409


@signin_bp.route("", methods=["POST"])
def sign_in():
    body = request.json
    email = body.get("email")
    password = body.get("password")

    users = Table("users", MetaData(bind=get_engine()), autoload=True)
    user_information = run_query(select(users).where(users.c.email == email))

    verify = sha256_crypt.verify(password, user_information[0]["password"])
    if verify == False or len(user_information) == 0:
        return {"message": "error, Email or password is incorrect"}, 401

    else:
        key = secrets.token_hex(16)
        body["token"] = jwt.encode(
            {"email": user_information[0]['email'], "type:": user_information[0]['type']}, key, "HS256")
        token = {"token": body["token"]}
        run_query(update(users).where(users.c.email == email).values(token), commit=True)
        return {
            "user_information":
                {
                    "name": user_information[0]['name'],
                    "email": user_information[0]['email'],
                    "phone_number": user_information[0]['phone_number'],
                    "type": user_information[0]['type']
                },
            "token": body["token"],
            "message": "Login success"
        }, 200
