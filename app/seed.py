from sqlalchemy import insert, Table, MetaData
from utils import get_engine, run_query
from passlib.hash import sha256_crypt

import uuid

def create_seeder():
    users = Table("users", MetaData(bind=get_engine()), autoload=True)

    seller = [
        {
            "id": str(uuid.uuid4()),
            "name": "Jose",
            "email": "jose@gmail.com",
            "password": sha256_crypt.hash("12345"),
            "phone_number": "0822123",
            "token": None,
            "type": "seller",
            "balance": "10000"
        }
    ]

    run_query(insert(users).values(seller), commit=True)

    categories = Table("categories", MetaData(bind=get_engine()), autoload=True)

    category = [
        {
            "id": str(uuid.uuid4()),
            "title": "T-Shirt",
            "image": "/image/T-shirt_83.jpg",
            "deleted": False
        },
        {
            "id": str(uuid.uuid4()),
            "title": "Shoes",
            "image": "/image/Sneakers_95.jpg",
            "deleted": False
        },
        {
            "id": str(uuid.uuid4()),
            "title": "Trouser",
            "image": "/image/Trouser_51.jpg",
            "deleted": False
        },
        {
            "id": str(uuid.uuid4()),
            "title": "Shirt",
            "image": "/image/Shirt_94.jpg",
            "deleted": False
        }
        
    ]
    

    run_query(insert(categories).values(category), commit=True)

    banners = Table("banners", MetaData(bind=get_engine()), autoload=True)

    banner = [
        {
            "id": str(uuid.uuid4()),
            "title": "banner",
            "image": "/image/1.png",
            "deleted": False
        },
        {
            "id": str(uuid.uuid4()),
            "title": "banner",
            "image": "/image/2.png",
            "deleted": False
        },
        {
            "id": str(uuid.uuid4()),
            "title": "banner",
            "image": "/image/3.png",
            "deleted": False
        }
        
    ]

    run_query(insert(banners).values(banner), commit=True)

    products = Table("products", MetaData(bind=get_engine()), autoload=True)

    product = [
        {
            "id": str(uuid.uuid4()),
            "title": "tshirt",
            "image": "/image/T-shirt_83.jpg",
            "price": 1000,
            "category_id":  category[0]["id"],
            "product_detail": "tshirt",
            "condition": "used",
            "deleted": False,
            "sold": 10
        },
        {
            "id": str(uuid.uuid4()),
            "title": "tshirt1",
            "image": "/image/T-shirt_83.jpg",
            "price": 1000,
            "category_id":  category[0]["id"],
            "product_detail": "tshirt",
            "condition": "used",
            "deleted": False,
            "sold": 10
        },
        {
            "id": str(uuid.uuid4()),
            "title": "sepatu",
            "image": "/image/Sneakers_95.jpg",
            "price": 1000,
            "category_id":  category[1]["id"],
            "product_detail": "sepatu",
            "condition": "used",
            "deleted": False,
            "sold": 10
        },
        {
            "id": str(uuid.uuid4()),
            "title": "Trouser",
            "image": "/image/Trouser_51.jpg",
            "price": 1000,
            "category_id":  category[2]["id"],
            "product_detail": "trouser",
            "condition": "used",
            "deleted": False,
            "sold": 10
        },
        {
            "id": str(uuid.uuid4()),
            "title": "Shirt",
            "image": "/image/Shirt_94.jpg",
            "price": 1000,
            "category_id":  category[3]["id"],
            "product_detail": "kemeja",
            "condition": "used",
            "deleted": False,
            "sold": 10
        }
        
    ]

    run_query(insert(products).values(product), commit=True)

    banners = Table("banners", MetaData(bind=get_engine()), autoload=True)

    banner = [
        {
            "id": str(uuid.uuid4()),
            "product_id": product[0]["id"],
            "size": "S"
        },
        {
            "id": str(uuid.uuid4()),
            "product_id": product[0]["id"],
            "size": "M"
        },
        {
            "id": str(uuid.uuid4()),
            "product_id": product[0]["id"],
            "size": "L"
        }
        
    ]