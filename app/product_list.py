"""
Product List.

Pada menu Product List, terdapat 3 (tiga) endpoints yang
perlu diselesaikan oleh peserta, sebagai berikut:

[Get Product List]
- Bobot nilai: 1 poin
- URL: /products
- method: GET
- Query Parameters:
    - page: integer (required) -> 1
    - page_size: integer (required) -> 100
    - sort_by: string (required) -> Price a_z, Price z_a
    - category: string (required) -> Id category a, id category b
    - price: integer (required) -> 0,10000
    - condition: string (required) -> used
    - product_name: string (required) -> name

Requirements (from the earliest to check):
- Jika produk sudah dihapus atau "category" sudah tidak available:
    - return {"message": "error, Item is not available"}
    - status code: 400}
- If everything is valid:
    - return {
        "data" : [
            {
                "id": "uuid",
                "image": "/something/image.png",
                "title": "Item A",
                "price": 15000
            }
        ],
        "total_rows" : 10
    }
    - status code: 200
- NOTE:
total_rows adalah berapa banyak total hasil yang didapatkan dengan semua filter yang ada

[Get Category]
- Bobot nilai: 0.5 poin
- URL: /categories
- method: GET

Requirements (from the earliest to check):
- If everything is valid:
    - return {
        "data" : [
            {
                "id": "uuid",
                "title": "Category A"
            }
        ]
    }
    - status code: 200

[Search Product by Image]
- Bobot nilai: 1 poin
- URL: /products/search_image
- method: POST
- Query Parameters:
    - image: string (required) -> Base_64 image

Requirements (from the earliest to check):
- If everything is valid:
    - return {"category_id" : "uuid"}
    - status code: 200
"""
import base64

from flask import Blueprint, request
from sqlalchemy import Table, MetaData, select, desc, asc, and_
from utils import get_engine, run_query

# this means that you can group all endpoints with prefix "/products" together
products_bp = Blueprint("products", __name__, url_prefix="/products")

# this means that you can group all endpoints with prefix "/categories" together
categories_bp = Blueprint("categories", __name__, url_prefix="/categories")


@products_bp.route("", methods=["GET"])
def get_product():
    args = request.args
    sort_by = args.get("sort_by")
    category = args.get("category")
    product_name = args.get("product_name")
    condition = args.get("condition")
    page = args.get("page")
    page_size = args.get("page_size")
    price = args.get("price")

    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    images = Table("images", MetaData(bind=get_engine()), autoload=True)

    if len(args) == 0:
        product_list = run_query(select(products))
    elif category == None and product_name == None and condition == None and price == None:
        product_list = run_query(select(products).limit(int(page)*int(page_size)))
    else:      
        if sort_by == "price a_z":
            sort_by = asc(products.c.price)
        else:
            sort_by = desc(products.c.price)

        product_list = run_query(select(products).where(and_(products.c.category_id == category)))

    data = []
    for i in range(len(product_list)):
        dict = {}

        
        if product_list[i]["image"] == None:
            dict["image"] = "/image/no-image.jpg"
        else:
            dict["image"] = product_list[i]["image"] 

        dict["id"] = product_list[i]["id"]
        dict["title"] = product_list[i]["title"]
        dict["price"] = product_list[i]["price"]

        data.append(dict)

    return {"data": data, "total_rows": len(product_list)}, 200


@ categories_bp.route("", methods=["GET"])
def get_category():

    categories = Table("categories", MetaData(
        bind=get_engine()), autoload=True)

    get_category = run_query(select(categories))

    data = []
    for i in range(len(get_category)):
        dict = {}
        dict["id"] = get_category[i]["id"]
        dict["title"] = get_category[i]["title"]
        data.append(dict)

    return {"data": data}, 200


@ products_bp.route("/search_image", methods=["POST"])
def search_image():
    args = request.args
    image = args.get("image")

    base64_bytes = image.encode("ascii")
    sample_string_bytes = base64.b64decode(base64_bytes)
    image = sample_string_bytes.decode("ascii")

    images = Table("images", MetaData(bind=get_engine()), autoload=True)
    image = run_query(select(images).where(images.c.images_url == image))
    products = Table("products", MetaData(bind=get_engine()), autoload=True)
    data = run_query(select(products).where(
        products.c.id == image[0]["product_id"]))

    return {"category_id": f"{data[0]['category_id']}"}
